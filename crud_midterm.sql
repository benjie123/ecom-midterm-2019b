-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 19, 2019 at 11:36 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crud_midterm`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_crud`
--

CREATE TABLE `tbl_crud` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(250) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Contact` int(50) NOT NULL,
  `company` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_crud`
--

INSERT INTO `tbl_crud` (`id`, `name`, `address`, `Email`, `Contact`, `company`) VALUES
(44, 'dkjsdjaskdj', 'kjsdakdj', 'kjdaskjdk', 0, 'jkdjsak'),
(45, 'dkasjk', 'jkdsdjk', 'jkdjakj', 0, 'djaks'),
(46, 'dkasjdk', 'jkdjaskj', 'kdjksj1k', 0, 'kjdksjk'),
(47, 'dskajk', 'jkdjk', 'jkdjk', 0, 'da'),
(48, 'sdkasjdk', 'jdkasjk', 'jdkasjk', 0, 'jdkjk'),
(49, 'skdjsakj', 'kdjkj', 'kjdkj', 0, 'jd'),
(50, 'skdaskj', 'kdjksj', 'kdjkajd', 0, 'kdkjask'),
(51, 'sadk', 'jkdjaskdjk', 'jdkasjd', -31039, 'ksjdaksdjk'),
(52, 'dksajk', 'jkdjsk', 'jkdsjak', 93102319, 'fkaskld'),
(53, 'dsakj', 'kdjsakj', 'kdajskd', 0, 'dasdas');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_crud`
--
ALTER TABLE `tbl_crud`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_crud`
--
ALTER TABLE `tbl_crud`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
