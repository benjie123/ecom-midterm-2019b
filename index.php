<?php
	require_once("perpage.php");	
	require_once("db.php");
	$db_handle = new DBController();
	
	$name = "";
	$code = "";
	
	$queryCondition = "";
	
	$orderby = " ORDER BY id desc"; 
	$sql = "SELECT * FROM tbl_crud " . $queryCondition;
	$href = 'index.php';					
		
	$perPage = 10; 
	$page = 1;
	if(isset($_POST['page'])){
		$page = $_POST['page'];
	}
	$start = ($page-1)*$perPage;
	if($start < 0) $start = 0;
		
	$query =  $sql . $orderby .  " limit " . $start . "," . $perPage; 
	$result = $db_handle->runQuery($query);
	
	if(!empty($result)) {
		$result["perpage"] = showperpage($sql, $perPage, $href);
	}
?>
<html>
	<head>

	<link href="style.css" type="text/css" rel="stylesheet" />
	</head>
	<body>
<h2>Midterm Crud </h2>
		<div style="text-align:right;margin:20px 0px 10px; ">
		<a id="btnAddAction" href="add.php">Add New</a>
		</div>
    <div id="toys-grid">      
			<form name="frmSearch" method="post" action="index.php">
			
			
			<table cellpadding="10" cellspacing="5">
        <thead>
					<tr>
                   <th><strong>Name</strong></th>
                     <th><strong>Address</strong></th>          
                    <th><strong>Email</strong></th>
					<th ><strong>Contact</strong></th>
					<th><strong>Company</strong></th>
					<th><strong>ACtion</strong></th>
					
					</tr>
				</thead>
				<tbody>
					<?php
					if(!empty($result)) {
						foreach($result as $k=>$v) {
						  if(is_numeric($k)) {
					?>
          <tr>
					<td><?php echo $result[$k]["name"]; ?></td>
                    <td><?php echo $result[$k]["address"]; ?></td>
					<td><?php echo $result[$k]["Email"]; ?></td>		
					<td><?php echo $result[$k]["Contact"]; ?></td>
					<td><?php echo $result[$k]["company"]; ?></td> 
					<td>
				            <a ></a>	<a class="btnEditAction" href="edit.php?id=<?php echo $result[$k]["id"]; ?>">Edit</a> <a class="btnDeleteAction" href="delete.php?action=delete&id=<?php echo $result[$k]["id"]; ?>">Delete</a>   <a class="btnEditAction" href="View.php?id=<?php echo $result[$k]["id"]; ?>">View</a>
					</td>
					</tr>
					<?php
						  }
					   }
                    }
					if(isset($result["perpage"])) {
					?>
					<tr>
					<td colspan="6" align=center> <?php echo $result["perpage"]; ?></td>
					</tr>
					<?php } ?>
				<tbody>
			</table>
			</form>	
		</div>
	</body>
</html>